let myMap = L.map('map').setView([48.50, 32.60], 6)

// Basemap tiles
let openStreetMap = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  minZoom: 3,
  maxZoom: 19,
  attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(myMap)
// Set extend limitation
let southWest = L.latLng(53, 21)
let northEast = L.latLng(44, 41)
let bounds = L.latLngBounds(southWest, northEast)
myMap.setMaxBounds(bounds);
myMap.on('drag', function () {
  myMap.panInsideBounds(bounds, { animate: true });
})

// Create layer groups
let occupiedGroupB = L.layerGroup().addTo(myMap)
let occupiedGroupA = L.layerGroup().addTo(myMap)
let explGroup = L.layerGroup().addTo(myMap)
let fightGroup = L.layerGroup().addTo(myMap)

// Add GeoJSON dat
let occupiedBefore = 'https://gitlab.com/fbk-team/backend/rudata/-/raw/sit_map/Occupied_before.geojson' // Territories occupied before Feb 24
let occupiedAfter = 'https://gitlab.com/fbk-team/backend/rudata/-/raw/sit_map/Occupied_new.geojson' // Territories occupied after Feb 24
let shelling = 'https://gitlab.com/fbk-team/backend/rudata/-/raw/sit_map/Shelling.geojson' // Shelling points
let activeFight = 'https://gitlab.com/fbk-team/backend/rudata/-/raw/sit_map/Active_fight.geojson' // Active fight points

let beforeStyle = {
  'fillColor': '#F56758', // fill color
  'fillOpacity': 0.50, // fill opacity
  'color': '#4D4C4C', // outline color
  'weight': 0.5, // outline width
  'opacity': 0.5 // outline opacity
}

let afterStyle = {
  'fillColor': '#D2B8B3', // fill color
  'fillOpacity': 0.50, // fill opacity
  'color': '#4D4C4C', // outline color
  'weight': 0.5, // outline width
  'opacity': 0.5 // outline opacity
}

// Polygon layer for Territories occupied BEFORE Feb 24
jQuery.getJSON(occupiedBefore, function (data) {
  L.geoJSON(data, {
    // onEachFeature: regionNames,
    style: beforeStyle
  }).addTo(occupiedGroupB)
})

// Polygon layer for Territories occupied AFTER Feb 24
jQuery.getJSON(occupiedAfter, function (data) {
  L.geoJSON(data, {
    style: afterStyle
  }).addTo(occupiedGroupA)
})

// Point layer for Shelling points
jQuery.getJSON(shelling, function (data) {
  let explosionIcon = L.icon({
    iconUrl: 'icons/ExplosionLarge.png',
    iconSize: [50, 40] // width, height
  });
  L.geoJSON(data, {
    pointToLayer: function (feature, latlng) {
      return L.marker(latlng, {
        icon: explosionIcon
      })
    }
  }).addTo(explGroup)
})

// Point layer for Active fight points
jQuery.getJSON(activeFight, function (data) {
  let fireIcon = L.icon({
    iconUrl: 'icons/FireLarge.png',
    iconSize: [30, 40] //  width, height
  });
  L.geoJSON(data, {
    pointToLayer: function (feature, latlng) {
      return L.marker(latlng, {
        icon: fireIcon
      })
    }
  }).addTo(fightGroup)
})

// add pop-ups
// let regionNames = function (feature, layer) {
//   let region = feature.properties.name_rus
//   let capital = feature.properties.capital_ru
//   layer.bindPopup(
//     '<b>Регион: </b>' + region +
//     '<br><b>Столица: </b>' + capital)
//   regionsRussia.addLayer(layer)
// }

let baseLayers = {
  'OpenStreetMap': openStreetMap
}

let overlays = {
  'Окупировано до 24-го февраля': occupiedGroupB,
  'Окупировано после 24-го февраля': occupiedGroupA,
  'Активные бои': fightGroup,
  'Обстрелы': explGroup
}

L.control.layers(baseLayers, overlays).addTo(myMap);
